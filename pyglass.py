#!/usr/bin/env python

# copyright (2017) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
# Licence:XXXXXXXXXXXX
# Authors:
#   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>
#   Eric Marin-Lamellet <eric.marin-lamellet@geoazur.unice.fr>
__VERSION__ = "3.2.1"

import requests
import sys
import os
import logging
from configparser import ConfigParser
from urllib.request import urlretrieve, urlcleanup
from urllib.error import URLError
from os.path import join as pjoin, exists
from os import makedirs
from datetime import datetime
import socket
import concurrent.futures
import logging
import certifi
import hashlib

logger = logging.getLogger(__name__)

# Set log level
logger.setLevel(logging.INFO)

# Create formatter and handler
formatter = logging.Formatter(
    "%(asctime)s - [%(filename)s: line %(lineno)s - function %(funcName)s() ] - %(levelname)s:  %(message)s"
)
handler = logging.StreamHandler()
handler.setFormatter(formatter)

# Add handler to logger
logger.addHandler(handler)


def build_url(
    protocol: str,
    hostname: str,
    filename: str,
    structure: str = None,
    relative_path: str = None,
) -> str:

    if not all(isinstance(arg, str) for arg in [protocol, hostname, filename]):
        raise TypeError("Protocol, hostname, and filename must be strings.")

    if protocol.find("://") >= 0:
        protocol = protocol.split("://")[0]

    hostname = hostname.strip("/")
    filename = filename.strip("/")

    if structure:
        structure = structure.strip("/")
    else:
        structure = ""
    if relative_path:
        if relative_path[0] == "/" and len(relative_path) > 1:
            relative_path = relative_path[1:]
        elif relative_path[0] == "/":
            relative_path = ""
        if relative_path != "" and relative_path[-1] == "/":
            relative_path = relative_path[:-1]

    else:
        relative_path = ""

    if structure == "" and relative_path == "":
        url = f"{protocol}://{hostname}/{filename}"
    elif structure == "":
        url = f"{protocol}://{hostname}/{relative_path}/{filename}"
    elif relative_path == "":
        url = f"{protocol}://{hostname}/{structure}/{filename}"
    else:
        url = f"{protocol}://{hostname}/{structure}/{relative_path}/{filename}"

    return url


def url_logs(short_json, type="GeodesyML"):
    url_list = []
    url_base = "http://dgw.unice.fr:8080/GlassFramework/webresources/log/"

    for s in short_json:
        if type == "GeodesyML":
            url = pjoin(url_base, "geodesyml/" + s["marker_long_name"])
        else:
            url = pjoin(url_base, s["marker_long_name"])
        url_list.append(url)
    return url_list


class PyglassError(Exception):
    """Base class for exceptions in this module."""

    pass


class ConfigError(Exception):
    """Base class for exceptions in this module."""

    def __init__(self, error_message):
        self.message = error_message

    def __str__(self):
        return repr(self.message)


class PyglassURLError(PyglassError):
    """
    Exception raised when there is an error in a URL
    """

    def __init__(self, num_error, url):
        self.num_error = num_error
        self.url = url
        self.message = "%s Error in URL: %s" % (self.num_error, self.url)

    def __str__(self):
        return repr(self.message)


class UrlMan:
    """
    Class for cleaning URLs
    """

    def __init__(self, root_url):
        self.url = self.clean(root_url)
        self.is_root = True

    def clean(self, url):
        """
        Clean the URL (remove trailing '/' for the moment)
        :param url: string
        :return: string
        """
        if url is None:
            return ""
        elif url[0] == "/":
            return url[1:]
        else:
            return url

    def join(self, url):
        """
        Join 2 part of a URL: the one of the server and the ones of the
        :param url:
        :return:
        """
        if self.url[-1] == "/":
            slash = ""
        else:
            slash = "/"
        self.url = self.url + slash + self.clean(url)

    def add_param(self, name, values):
        """
        Add params to a URL
        :param name: string of the parameter to query
        :param values: list of values for the parameters
        :return: nothing
        """
        separator = "="

        if self.is_root == True:
            if self.url[-1] not in ["?", "/"]:
                logger.error(f"bad url construction")
            self.is_root = False
            esper = ""
        else:
            esper = "&"

        if isinstance(values, str):
            self.url = (
                self.url
                + esper
                + name
                + separator
                + values.replace("/", "%2F").replace("&", "%26")
            )
        else:
            self.url = (
                self.url
                + esper
                + name
                + separator
                + ",".join(x.replace("/", "%2F").replace("&", "%26") for x in values)
            )

    def __str__(self):
        return repr(self.url)


class Pyglass:
    """
    Main class
    """

    def __init__(self, query_params):
        self.query_params = query_params
        self.server_url = self.query_params.pop("server_url")
        self.dgw_backend_url = self.query_params.pop("dgw_backend_url")
        self.all_stations = self.query_params.pop("all")
        self.output = self.query_params.pop("output")
        self.download_dir = self.query_params.pop("download-dir")
        self.highrate = self.query_params.pop("highrate")
        self.type_query = self.query_params.pop("target")

    def remind_params(self):
        """
        Print the list of parameters.
        :return: None
        """
        for key, item in self.query_params.items():
            print(f"{key}:  {item}")

        if self.download_dir is not None:
            print(f"download_dir: {self.download_dir}")

        if self.output is not None:
            print(f"output: {self.output}")

    def build_query(self):
        """
        Build the query URL
        :return: None
        """

        self.query = UrlMan(self.server_url)

        if self.all_stations is True and self.highrate:
            self.query.join("/webresources/stations/v2/highrate/bbox/-180/-90/180/90")
        elif self.all_stations is True:
            self.query.join("/webresources/stations/v2/combination/short/json")
        else:
            self.query.join("/webresources/")
            if self.type_query == "stations" and self.highrate:
                self.query.join("stations/v2/highrate/bbox/")
            elif self.type_query == "stations":
                self.query.join("stations/v2/combination")
                if self.output in [None, "full-json"]:
                    self.query.join("full/json?")
                elif self.output in [
                    "SiteLog",
                    "GeodesyML",
                    "short-json",
                    "station-list",
                ]:
                    self.query.join("short/json?")
                elif self.output == "full-xml":
                    self.query.join("full/xml?")
                elif self.output == "full-csv":
                    self.query.join("full/csv?")
                elif self.output == "short-csv":
                    self.query.join("short/csv?")
            elif self.type_query == "files" and self.highrate:
                self.query.join("files/highrate/station-marker/")
            elif self.type_query == "files":
                self.query.join("files/combination/")

            if self.highrate and self.type_query == "stations":
                self.query.join(
                    f'{self.query_params["minLon"]}/{self.query_params["minLat"]}/{self.query_params["maxLon"]}/{self.query_params["maxLat"]}'
                )
            elif self.highrate and self.type_query == "files":
                marker_str = ",".join(self.query_params["marker"])
                self.query.join(marker_str)
            else:
                for key, value in self.query_params.items():
                    if value not in [None, "", [], ()]:
                        self.query.add_param(key, value)

            if self.type_query == "files":
                if self.output in [None, "full-json"]:
                    self.query.join("json")
                elif self.output in [
                    "short-json",
                    "rinex-list",
                    "download",
                ]:
                    self.query.join("json")
                elif self.output in ["short-csv", "full-csv"]:
                    self.query.join("csv")
                elif self.output == "full-xml":
                    self.query.join("xml")

    def query_info(self):
        """
        print the query URL
        :return: None
        """
        print(self.query.url)

    def run_query(self):
        """
        run the query
        :return: string: the result of the query
        """
        try:
            if self.highrate and self.type_query == "stations" and self.output:
                if self.output == "full-csv" or self.output == "short-csv":
                    res = requests.get(
                        self.query.url, headers={"accept": "application/csv"}
                    )
                else:
                    res = requests.get(
                        self.query.url, headers={"accept": "application/json"}
                    )
            elif self.highrate and self.type_query == "stations":
                res = requests.get(
                    self.query.url, headers={"accept": "application/json"}
                )
            else:
                res = requests.get(self.query.url)
        except Exception as e:
            logging.error(e)
            sys.exit()

        # Error handling
        if res.status_code == 204:
            print("No data available")
            sys.exit()
        elif res.status_code != 200:
            raise PyglassURLError(num_error=res.status_code, url=self.query.url)
        else:
            res.encoding = "UTF-8"  # if no encoding, res.text is vey slow
            return res.text

    def get_station_doi_license(self, marker_stations=None):
        """Get and parse the dgw backend json for a station to get the license, and the doi.

        Args:
            station list: list of the stations

        Returns : list of dict with the station licences an doi

        """
        station_doi_license = {}

        for station in marker_stations:
            url = self.dgw_backend_url.replace("<9CHAR_ID>", station)
            res_json = requests.get(url).json()

            if station == res_json["id"]:
                station_doi_license[station] = {
                    "licence": res_json["properties"]["license"],
                    "doi": res_json["properties"]["doi"],
                }
            else:
                print(station, "not found")

        return station_doi_license


class ProcessArgs:
    """process the command line arguments"""

    def __init__(self, parser, args):
        self.parser = parser
        self.args = args
        self.has_config_file = False
        self.server_url = None
        self.dico_param = {}

    def help_exit(self, msg=None):
        """
        command line args error message
        :param msg: string (additional message)
        :return: None
        """
        self.parser.print_help()
        self.parser.exit(msg)

    def process_args(self):
        # Load the config file if existing
        # it has to be in the current directory
        if (self.args.target != "stations") and (self.args.target != "files"):
            self.help_exit("You must choose a target: stations or files")

        if self.args.highrate and self.args.target == "stations":
            if self.args.markers:
                self.help_exit(
                    "Option markers are not compatible with stations target and higrate option"
                )
            elif (
                not self.args.min_lat
                and not self.args.min_lon
                and not self.args.max_lat
                and not self.args.max_lon
            ):
                if not self.args.all:
                    self.help_exit(
                        "You must specify minimal latitude and longitude and maximal latitude and longitude, or --all to get the stations metadata"
                        "for all the stations with highrate files"
                    )
        elif self.args.highrate and self.args.target == "files":
            if not self.args.markers:
                self.help_exit("You must specify markers stations")

        if self.args.highrate and (
            self.args.circle_select
            or self.args.polygon_select
            or self.args.site_name
            or self.args.min_alt
            or self.args.max_alt
            or self.args.installed_date_from
            or self.args.installed_date_to
            or self.args.network
            or self.args.inverse_networks
            or self.args.agency
            or self.args.satellite
            or self.args.receiver_type
            or self.args.antenna_type
            or self.args.radome_type
            or self.args.country
            or self.args.province
            or self.args.city
            or self.args.data_date_start
            or self.args.data_date_end
            or self.args.published_date_start
            or self.args.published_date_end
            or self.args.data_availability
            or self.args.minimum_observation_years
            or self.args.sampling_window
            or self.args.sampling_frequency
            or self.args.status_files
            or self.args.file_type
        ):
            self.help_exit("this query is not available for highrate file. ")

        if self.args.output:
            if self.args.output not in [
                "short-json",
                "full-json",
                "short-csv",
                "full-csv",
                "full-xml",
                "station-list",
                "rinex-list",
                "download",
                "GeodesyML",
                "SiteLog",
            ]:
                self.help_exit(
                    "invalid choice for -o: you must choose from short-json, full-json, short-csv, full-csv, full-xml, station-list, rinex-list, download, GeodesyML, SiteLog"
                )

        if (self.args.download_dir) and self.args.target == "stations":
            if self.args.output not in ["SiteLog", "GeodesyML"]:
                self.help_exit(
                    '--download-dir is incompatible with target "stations" if ouptut is not set to SiteLog or GeodesyML'
                )

        if self.args.output and self.args.target == "stations":
            if self.args.output == "download":
                self.help_exit(
                    'Output "download" is incompatible with target "stations"'
                )
            if self.args.output == "rinex-list":
                self.help_exit(
                    'Output "rinex-list" is incompatible with target "stations"'
                )

        elif self.args.output and self.args.target == "files":
            if self.args.output in ["SiteLog", "GeodesyML", "station-list"]:
                self.help_exit(
                    f'Output "{self.args.output}" is incompatible with target "files"'
                )

        if self.args.config_file:
            config = ConfigParser()
            config.read(self.args.config_file)
            self.has_config_file = True
        elif os.path.isfile(os.path.join(os.path.dirname(__file__), "pyglass.ini")):
            config = ConfigParser()
            config.read(os.path.join(os.path.dirname(__file__), "pyglass.ini"))
            self.has_config_file = True

        if self.has_config_file:
            # Read the config file: server address,
            try:
                self.server_default = config.get("Default", "server_default")
                if config.has_option("Default", "server_default"):
                    # Read the default server
                    self.server_default = config.get("Default", "server_default")

                if config.has_option("ServerList", self.server_default):
                    self.server_url = config.get("ServerList", self.server_default)
                if config.has_option("DGW_BACKEND_API", "dgw_backend"):
                    self.dgw_backend_url = config.get("DGW_BACKEND_API", "dgw_backend")
                else:
                    raise ConfigError('missing "dgw backend" in the config file')
            except Exception as e:
                self.help_exit("Unable to parse config file")

        if self.args.url:
            self.server_url = self.args.url

        if not self.server_url:
            self.help_exit(
                "No url (either in a configuration file or as a command line option) is present"
            )

        if self.args.all is True and self.args.target != "stations":
            self.help_exit("--all compatible only with target: stations")

        # Managing the geographical selections
        coord_dict = None

        if self.args.rect_select:
            coord_dict = self.process_rectangle(self.args.rect_select)

        if self.args.circle_select:
            coord_dict = self.process_circle(self.args.circle_select)

        if self.args.polygon_select:
            coord_dict = self.process_polygon(self.args.polygon_select)

        data_range = self.process_data_date_range(
            self.args.data_date_start, self.args.data_date_end
        )

        if (
            self.args.published_date_start or self.args.published_date_end
        ) and self.args.target == "stations":
            self.help_exit('Published date range incompatible with target "stations"')

        # published date range
        published_range = self.process_published_date_range(
            self.args.published_date_start, self.args.published_date_end
        )

        if not self.args.download_dir:
            download_dir = "./"
        else:
            download_dir = self.args.download_dir

        # parameters to pass to the query builder
        # mostly list of the parameters and their values to build the URL with.
        self.dico_param = {
            "server_url": self.server_url,
            "dgw_backend_url": self.dgw_backend_url,
            "target": self.args.target,
            "marker": self.args.markers,
            "site": self.args.site_name,
            "min_alt": self.args.min_alt,
            "max_alt": self.args.max_alt,
            "minLat": self.args.min_lat,
            "minLon": self.args.min_lon,
            "maxLat": self.args.max_lat,
            "maxLon": self.args.max_lon,
            "installedDateMin": self.args.installed_date_from,
            "installedDateMax": self.args.installed_date_to,
            "network": self.args.network,
            "invertedNetworks": self.args.inverse_networks,
            "agency": self.args.agency,
            "satellite": self.args.satellite,
            "receiver": self.args.receiver_type,
            "antenna": self.args.antenna_type,
            "radome": self.args.radome_type,
            "country": self.args.country,
            "state": self.args.province,
            "city": self.args.city,
            "dateRange": data_range,
            "published_date": published_range,
            "dataAvailability": self.args.data_availability,
            "minimumObservationYears": self.args.minimum_observation_years,
            "samplingWindow": self.args.sampling_window,
            "samplingFrequency": self.args.sampling_frequency,
            "statusfile": self.args.status_files,
            "fileType": self.args.file_type,
            "output": self.args.output,
            "download-dir": download_dir,
            "all": self.args.all,
            "highrate": self.args.highrate,
        }

        if coord_dict is not None:
            self.dico_param.update(coord_dict)

        if self.args.all is False:
            dico_empty = True
            # Check if values to query stored in dico_param are not empty (None, '', or [])
            # exception are "all" and "server_url" which are not values to query
            for key, item in self.dico_param.items():
                if key not in ["all", "server_url"] and (item in [None, "", []]):
                    dico_empty = True
                elif key in ["all", "server_url"]:
                    continue
                else:
                    dico_empty = False
                    break
            if dico_empty:
                self.help_exit(
                    "You need to select parameters for the query.\n --all to download the short-json for all the stations."
                )

    def process_rectangle(self, selection):
        if len(selection) != 4:
            self.help_exit("Error in rectangular selection. Check coordinates")
        else:
            for i in selection:
                try:
                    float(i)
                except Exception as e:
                    self.help_exit(e.message)

            minLat, maxLat, minLon, maxLon = selection
            coord_dict = {
                "coordinates": "rectangle",
                "minLat": minLat,
                "minLon": minLon,
                "maxLat": maxLat,
                "maxLon": maxLon,
            }
            return coord_dict

    def process_circle(self, selection):
        if len(selection) != 3:
            self.help_exit("Error in circle selection. Check coordinates")
        else:
            for i in selection:
                try:
                    float(i)
                except Exception as e:
                    self.help_exit(e.message)
            centerLat, centerLon, radius = selection
            coord_dict = {
                "coordinates": "circle",
                "centerLat": centerLat,
                "centerLon": centerLon,
                "radius": radius,
            }
            return coord_dict

    def process_polygon(self, selection):
        if len(selection) % 2 != 0 or len(selection) < 6:
            self.help_exit("Error in the polygon selection. Check coordinates")
        else:
            for i in selection:
                try:
                    float(i)
                except Exception as e:
                    self.help_exit(e.message)

            polygon_coords = []
            temp_string = ""
            for i, coord in enumerate(selection):
                if i % 2 == 0:
                    temp_string = coord
                else:
                    polygon_coords.append(temp_string + "," + coord)
                    temp_string = ""
                coord_dict = {
                    "coordinates": "polygon",
                    "polygon": ";".join(polygon_coords),
                }
            return coord_dict

    def process_data_date_range(self, start, end):
        data_date_start = "" if not start else start
        data_date_end = "" if not end else end
        data_range = data_date_start + "," + data_date_end
        if data_range == ",":
            data_range = None
        return data_range

    def process_published_date_range(self, start, end):
        published_date_start = "" if not start else start
        published_date_end = "" if not end else end
        published_range = published_date_start + "," + published_date_end
        if published_range == ",":
            published_range = None
        return published_range


def parse_url(url):
    fname = url.split("/")[-1]


def download_file(url_targetdir):
    """
    Download a file from a given URL.

    Args:
        url (str): The URL of the file to download.
        download_dir (str): The directory in which to save the downloaded file.

    Returns: None
    """
    url, download_dir = url_targetdir
    try:
        fname = os.path.basename(url)
        print(f"Downloading {fname}...", f"{url}")
        response = requests.get(url, stream=True, verify=certifi.where())
        with open(os.path.join(download_dir, fname), "wb") as f:
            for chunk in response.iter_content(chunk_size=8192):
                if chunk:
                    f.write(chunk)
        print(f"Download of {fname} complete.")
        return None

    except Exception as e:
        logger.error(f"Error downloading file from {url}: {e}")
        return url


def download_parallel(url_targetdir_list, num_threads=None):
    """
    Downloads a list of files from the given URLs concurrently using multiple threads.

    Parameters:
        url_targetdir_list (list): a list of list of an url from which to download file
        and a directory in which to save the download file
        num_threads (int): The number of threads to use for the download process.

    Returns: None

    Prints the number of threads used during the download process.
    """
    errors = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=num_threads) as executor:
        futures = [
            executor.submit(download_file, url_targetdir)
            for url_targetdir in url_targetdir_list
        ]
        for future in concurrent.futures.as_completed(futures):
            result = future.result()
            if result is not None:
                errors.append(result)
        print(f"Number of threads used: {executor._max_workers}")
    return errors


def calculate_md5(file_path):
    """
    Calculate the MD5 checksum of a file.

    Args:
        file_path (str): The path to the file.

    Returns:
        str: The MD5 checksum of the file.
    """
    hash_md5 = hashlib.md5()
    try:
        with open(file_path, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
    except Exception as e:
        logger.error(e)
    return hash_md5.hexdigest()


def verify_md5(file_path, expected_md5):
    """
    Verify the MD5 checksum of a file.

    Args:
        file_path (str): The path to the file.
        expected_md5 (str): The expected MD5 checksum.

    Returns:
        bool: True if the checksum matches, False otherwise.
    """
    calculated_md5 = calculate_md5(file_path)
    return calculated_md5 == expected_md5


def main():
    """
    Main function. Used this way to be able to provide this tool as a single executable by using the fact that python can run zip files.
    :return: nothing
    """

    import argparse
    import os.path
    from datetime import datetime

    RUN_DATE = str(datetime.now().date())
    RUN_DATETIME = str(datetime.now().strftime("%Y-%m-%d_%H-%M-%S"))

    # .replace(' ', '_').replace(':', '-') # replace characters to be compatible with windows when used as  filename
    # superseeded by the usage
    # read command line
    # Warning: command line options supersede config file options

    parser = argparse.ArgumentParser(
        description="Command line client for a GLASS-API server. Note that space is used as delimiter."
    )
    parser.add_argument(
        "--version",
        dest="version",
        action="store_true",
        help=f"%(prog)s version : {__VERSION__}",
    )
    parser.add_argument(
        "target",
        action="store",
        help="stations or files. Choose between the station metadata or the file metadata.",
    )
    parser.add_argument("-u", dest="url", action="store", help="GLASS server url")
    parser.add_argument(
        "-cf",
        dest="config_file",
        nargs="?",
        action="store",
        const=os.getcwd() + "/pyglass.ini",
        help="Use a configuration file. Defalut path Path to configuration file",
    )

    parser.add_argument(
        "-m",
        dest="markers",
        action="store",
        nargs="+",
        help="4 or 9 characters code of the station. "
        "The option can take more than one marker"
        " and 1 to 9 characters per marker ",
    )

    parser.add_argument(
        "-sn",
        dest="site_name",
        action="store",
        nargs="+",
        help="Station names. The option can take more"
        " than one name. If a name has a space"
        " it should be quoted.",
    )
    coord_args = parser.add_mutually_exclusive_group()

    coord_args.add_argument(
        "-rs",
        dest="rect_select",
        action="store",
        nargs="+",
        help="rectangular selection (order: "
        "latitude min, latitude max, "
        "longitude min, longitude max)",
    )
    coord_args.add_argument(
        "-cs",
        dest="circle_select",
        action="store",
        nargs="+",
        help="circular selection (order: latitude, longitude, radius in km)",
    )
    coord_args.add_argument(
        "-ps",
        dest="polygon_select",
        action="store",
        nargs="+",
        help="polygon selection (order: "
        "point A latitude, point A "
        "longitude, point B latitude,"
        " point B longitude, point C latitude,  point C longitude (each point in order))",
    )

    # to be feature-compatible with the web client
    parser.add_argument(
        "-ltmin", dest="min_lat", action="store", help="Minimal latitude"
    )
    parser.add_argument(
        "-ltmax", dest="max_lat", action="store", help="Maximal latitude"
    )
    parser.add_argument(
        "-lgmin", dest="min_lon", action="store", help="Minimal longitude"
    )
    parser.add_argument(
        "-lgmax", dest="max_lon", action="store", help="Maximal longitude"
    )
    parser.add_argument("-htmin", dest="min_alt", action="store", help="Minimal height")
    parser.add_argument("-htmax", dest="max_alt", action="store", help="Maximal height")
    parser.add_argument(
        "-idmin",
        dest="installed_date_from",
        action="store",
        nargs=1,
        help="Station installed from ...",
    )
    parser.add_argument(
        "-idmax",
        dest="installed_date_to",
        action="store",
        nargs=1,
        help="Station installed up to ...",
    )
    parser.add_argument(
        "-rdmin",
        dest="removed_date_from",
        action="store",
        nargs=1,
        help="Station installed from ...",
    )
    parser.add_argument(
        "-rdmax",
        dest="removed_date_to",
        action="store",
        nargs=1,
        help="Station installed up to ...",
    )
    parser.add_argument(
        "-nw", dest="network", action="store", nargs="+", help="network"
    )
    parser.add_argument(
        "-inw",
        dest="inverse_networks",
        action="store",
        nargs="+",
        help="All networks except...",
    )
    parser.add_argument("-ag", dest="agency", action="store", nargs="+", help="agency")
    parser.add_argument(
        "-ss", dest="satellite", action="store", nargs="+", help="Satellite System"
    )
    parser.add_argument(
        "-rt", dest="receiver_type", action="store", nargs="+", help="Receiver type"
    )
    parser.add_argument(
        "-at", dest="antenna_type", action="store", nargs="+", help="Antenna type"
    )
    parser.add_argument(
        "-radt", dest="radome_type", action="store", nargs="+", help="Radome type"
    )
    parser.add_argument(
        "-co", dest="country", action="store", nargs="+", help="Country"
    )
    parser.add_argument(
        "-pv", dest="province", action="store", nargs="+", help="Province/State/Region"
    )
    parser.add_argument("-ct", dest="city", action="store", nargs="+", help="City")

    parser.add_argument(
        "-dds", dest="data_date_start", action="store", help="starting date for data"
    )
    parser.add_argument(
        "-dde", dest="data_date_end", action="store", help="end date for data"
    )

    parser.add_argument(
        "-pds",
        dest="published_date_start",
        action="store",
        help="starting date of publication for data",
    )

    parser.add_argument(
        "-pde",
        dest="published_date_end",
        action="store",
        help="end date of publication for data",
    )

    parser.add_argument(
        "-da",
        dest="data_availability",
        action="store",
        help="Data availability (in %%)",
    )
    parser.add_argument(
        "-sw",
        dest="sampling_window",
        action="store",
        help="Sampling_window of data (1hr, 30s)",
    )
    parser.add_argument(
        "-sf",
        dest="sampling_frequency",
        action="store",
        help="Sampling frequency for data (1s, 30s)",
    )
    parser.add_argument(
        "-ft", dest="file_type", action="store", help="File type (RINEX2, RINEX3)"
    )
    parser.add_argument(
        "-moy",
        dest="minimum_observation_years",
        action="store",
        help="Maximum amount of years with files",
    )
    parser.add_argument(
        "-fs", dest="status_files", action="store", help="Status of files (int -3..3)"
    )

    parser.add_argument(
        "-o",
        dest="output",
        action="store",
        help="Output Format (cf: GLASS API document)."
        " Available short-json, full-json, short-csv, full-csv,"
        "full-xml, station-list, rinex-list, download, GeodesyML, SiteLog",
    )
    parser.add_argument(
        "--download-dir",
        dest="download_dir",
        action="store",
        help="Select RINEX download directory ",
    )
    parser.add_argument(
        "--all", dest="all", action="store_true", help="Get all metadata in short json"
    )
    parser.add_argument(
        "--print-url", dest="print_url", action="store_true", help="Only print URL"
    )
    parser.add_argument(
        "--timeout",
        action="store",
        help="Set the timeout for downlading a file. By default: 5s",
    )
    parser.add_argument(
        "-nt",
        "--num-threads",
        metavar="N",
        type=int,
        help="The number of threads to use for the download process.",
    )
    parser.add_argument(
        "--highrate",
        dest="highrate",
        action="store_true",
        help="Get highrate rinex files when argument target is files (queries only with marker),"
        "or get stations with highrate rinex files when argument targert is stations (queries only with ltmin, ltmax, lgmin, lgmax)",
    )

    args = parser.parse_args()

    if args.version:
        print("Version:", __VERSION__)
        sys.exit(0)

    processed_args = ProcessArgs(parser, args)
    processed_args.process_args()
    data = Pyglass(query_params=processed_args.dico_param)
    res = data.build_query()

    marker_stations = []

    if args.print_url:
        data.query_info()
        sys.exit()

    # set the timeout for  downloading files
    if not args.timeout:
        timeout = 5
    else:
        try:
            timeout = int(args.timeout)
        except Exception as e:
            print(f"Timeout '{timeout}' should be a number ! (error message {e})")
            sys.exit()

    socket.setdefaulttimeout(timeout)  # set the default timeout for downloading files

    import json

    try:
        res = data.run_query()  # run the query

        if args.output in [None, "full-json", "short-json"]:
            parsed = json.loads(res)
            if args.target == "stations":
                for item in parsed:
                    marker_stations.append(item["marker_long_name"])
                    station_doi_license = data.get_station_doi_license(marker_stations)
                    if item['marker_long_name'] in station_doi_license.keys():
                        item["license"] = station_doi_license[item['marker_long_name']]["licence"]
                        item["doi"] = station_doi_license[item['marker_long_name']]["doi"]
            print(json.dumps(parsed, indent=4, sort_keys=True))

        elif args.output == "rinex-list":
            parsed = json.loads(res)
            if len(parsed) != 0:
                urls_l = []
                for item in parsed:
                    urls_l.append(
                        build_url(
                            protocol=item["data_center"]["protocol"],
                            hostname=item["data_center"]["hostname"],
                            structure=item["data_center"]["data_center_structure"][
                                "directory_naming"
                            ],
                            relative_path=item["relative_path"],
                            filename=item["name"],
                        )
                    )
                urls_l.sort()
                for i in urls_l:
                    print(i)
            else:
                print("No data available for this selection")
        elif args.output == "station-list":
            parsed = json.loads(res)
            station_list = [item["marker_long_name"] for item in parsed]
            station_list.sort()
            for i in station_list:
                print(i)

        elif args.output == "download":
            if args.highrate:
                rinex_metadata_dir = "HIGHRATE_RINEX_METADATA_" + RUN_DATETIME
            else:
                rinex_metadata_dir = "RINEX_METADATA_" + RUN_DATETIME
            parsed = json.loads(res)

            files_size = [sub["file_size"] for sub in parsed]
            total_files_size = sum(files_size) / 1000000
            print(
                f"nb of rinex files: {len(parsed)}\nTotal size: {total_files_size} Mb\n"
            )

            while True:
                user_continue_process = input("Do you want to continue ? (y/n)\n")
                if user_continue_process not in ["y", "n"]:
                    print("Invalid input. Please enter y/n")
                elif user_continue_process == "y":
                    # Save metadata files
                    with open(rinex_metadata_dir, "w") as f:
                        f.write(json.dumps(parsed))

                    list_url_targetdir = []
                    list_download_file_path_md5_checksum = []
                    for item in parsed:
                        yyy, doy = (
                            datetime.strptime(
                                item["reference_date"].split()[0], "%Y-%m-%d"
                            )
                            .strftime("%Y-%j")
                            .split("-")
                        )

                        download_dir = pjoin(data.download_dir, pjoin(yyy, doy))
                        if not exists(download_dir):
                            makedirs(download_dir)
                        url = build_url(
                            protocol=item["data_center"]["protocol"],
                            hostname=item["data_center"]["hostname"],
                            structure=item["data_center"]["data_center_structure"][
                                "directory_naming"
                            ],
                            relative_path=item["relative_path"],
                            filename=item["name"],
                        )

                        download_file_path = os.path.join(download_dir, item["name"])
                        list_download_file_path_md5_checksum.append(
                            [download_file_path, item["md5_checksum"]]
                        )

                        list_url_targetdir.append([url, download_dir])
                    errors = download_parallel(
                        list_url_targetdir, num_threads=args.num_threads
                    )

                    for (
                        download_file_path_md5_checksum
                    ) in list_download_file_path_md5_checksum:
                        if verify_md5(
                            download_file_path_md5_checksum[0],
                            download_file_path_md5_checksum[1],
                        ):
                            print(
                                f"MD5 checksum verified successfully for {download_file_path_md5_checksum[0]}"
                            )
                        else:
                            print(
                                f"MD5 checksum verification failed for {download_file_path_md5_checksum[0]}"
                            )

                    if errors != []:
                        with open(
                            "download_error_" + RUN_DATE + ".log", "a"
                        ) as error_store:
                            for error in errors:
                                error_store.write(error + "\n")
                    break
                else:
                    sys.exit()

        elif args.output == "SiteLog" or args.output == "GeodesyML":
            parsed = json.loads(res)
            if len(parsed) != 0:
                log_list = url_logs(parsed, type=args.output)
                for log in log_list:
                    if not exists(data.download_dir):
                        makedirs(data.download_dir)
                    try:
                        if args.output == "GeodesyML":
                            urlretrieve(
                                log,
                                pjoin(data.download_dir, log.split("/")[-1] + ".zip"),
                            )
                        elif args.output == "SiteLog":
                            print(log)
                            urlretrieve(
                                log,
                                pjoin(
                                    data.download_dir,
                                    log.split("/")[-1].lower() + ".log",
                                ),
                            )
                        urlcleanup()
                    except Exception as e:
                        logger.error(f"File {log} - Error: {e}")

                for item in parsed:
                    marker_stations.append(item["marker_long_name"])
                station_doi_license = data.get_station_doi_license(marker_stations)
                with open("license_doi_" + RUN_DATETIME, "w") as f:
                    f.write(json.dumps(station_doi_license))
            else:
                print("No data available for this selection")

        else:
            # output = res.encode("utf-8").splitlines()
            # for line in output:
            #     print(line.decode("utf-8")
            with open(RUN_DATETIME + ".csv", "a") as f:
                f.write(res)

    except Exception as e:
        logger.error(e)
        sys.exit()


if __name__ == "__main__":
    main()
